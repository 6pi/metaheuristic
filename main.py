from utils import *
from graph import *
from enumeration import *
from recuit import *
from gradient import *
from tabou import *
import pandas as pd
import numpy as np
import os
from threading import Thread, Event
import time
import sys

__max_time__ = 2


def test_all_files():
	path = "./graphes/"

	files = os.listdir(path)
	print(files)
	k = 2
	for file in files:
		bridge = Event()
		gr = Graph(path + file, bridge)
		print(file)
		s = gr.init_sol(k)

		deter_thread = Thread(target=algoDeterministe, args=(gr, ))
		gradient_thread = Thread(target=gradient, args=(gr, s, voisinnage, ))
		recuit_thread = Thread(target=recuit, args=(gr, ))
		# We start the thread and will wait for 3 seconds then the code will continue to execute
		deter_thread.start()
		gradient_thread.start()
		recuit_thread.start()
		deter_thread.join(timeout=__max_time__)
		gradient_thread.join(timeout=__max_time__)
		recuit_thread.join(timeout=__max_time__)
		gr.bridge.set()
		del bridge
		del gr

def testAll(files):
	k = [ 2, 3, None]
	for j in k:
		print("\n[[[[[[[[ K = " + str(j) + "]]]]]]]]")
		for i in files:
			bridge = Event()
			tab_time = []
			obj = []
			gap_f = []
			obj_f = []
			time_f = []
			print("\n------------- " + i + "-------------\n")
			gr = Graph("sortedGraphes/" + i, j, bridge)
			tmp = -1
			s = gr.init_sol()
			for z in range(10):
				t_d = time.time()
				tab = gradient_standard(gr, s)
				t_f = time.time()
				tab_time.append(t_f - t_d)
				obj.append(tab[1])
			print("Time gradient : {} , Obj gradient : {}".format(np.mean(tab_time), np.mean(obj)))
			gap_f.append(math.fabs(np.mean(obj)-tmp)/tmp)
			obj_f.append(np.mean(obj))
			time_f.append(np.mean(tab_time))
			#gap_f.append("-")
			#obj_f.append("-")
			#time_f.append("-")
			tab_time = []
			obj = []
			s = gr.init_sol()
			for z in range(10):
				t_d = time.time()
				tab = gradient(gr, s, voisinnageRestreint_random_swap)
				t_f = time.time()
				tab_time.append(t_f - t_d)
				obj.append(tab[1])
			print("Time gradient Amelioré : {} , Obj gradient + : {}".format(np.mean(tab_time), np.mean(obj)))
			gap_f.append(math.fabs(np.mean(obj)-tmp)/tmp)
			obj_f.append(np.mean(obj))
			time_f.append(np.mean(tab_time))
			#gap_f.append("-")
			#obj_f.append("-")
			#time_f.append("-")
			s = tab[0]
			#s = gradient(gr, s, voisinnageRestreint_random_swap)
			tab_time = []
			obj = []
			for z in range(10):
				t_d = time.time()
				tab = recuit(gr, s=s)
				t_f = time.time()
				tab_time.append(t_f - t_d)
				obj.append(tab)
			print("Time Recuit : {} , Obj recuit : {}".format(np.mean(tab_time), np.mean(obj)))
			gap_f.append(math.fabs(np.mean(obj)-tmp)/tmp)
			obj_f.append(np.mean(obj))
			time_f.append(np.mean(tab_time))
			tab_time = []
			obj = []
			for z in range(10):
				t_d = time.time()
				tab = tabou(gr, x_best=s)
				t_f = time.time()
				tab_time.append(t_f - t_d)
				obj.append(tab)
			print("Time Tabou : {} , Obj tabou : {}".format(np.mean(tab_time), np.mean(obj)))
			gap_f.append(math.fabs(np.mean(obj)-tmp)/tmp)
			obj_f.append(np.mean(obj))
			time_f.append(np.mean(tab_time))
			#d = {"Algorithmes" : ["Enumeration", "Gradient", "Gradient +", "Recuit", "Tabou"], "Temps" : time_f, "Objectif" : obj_f, "Gap" : gap_f}
			d = {"Algorithmes" : ["Gradient", "Gradient +", "Recuit", "Tabou"], "Temps" : time_f, "Objectif" : obj_f, "Gap" : gap_f}
			i = i.split('.')[0][1:]
			#print(i)
			df = pd.DataFrame(data=d)
			df.to_csv(i + "PourK=" + str(j) + ".csv", index=False)
			print(i + " create")

def testEnum(files):
	k = [ 2, 3, None]
	for j in k:
		print("\n[[[[[[[[ K = " + str(j) + "]]]]]]]]")
		for i in files:
			if( i in ["l30Sommets.txt","m50Sommets.txt","n100Sommets.txt","o500Sommets.txt","p1000Sommets.txt","q10000Sommets.txt"]):
				continue
			bridge = Event()
			obj_f = []
			time_f = []
			print("\n------------- " + i + "-------------\n")
			gr = Graph("sortedGraphes/" + i, j, bridge)
			#gr.bridge.set()
			t_d = time.time()
			res = algoDeterministe(gr)
			t_f = time.time()
			print("Time enumeration : {}, obj enumeration: {}".format(str(t_f - t_d), res))
			obj_f.append(res)
			time_f.append(float(t_f - t_d))
			t_d = time.time()
			res = generateAndTest(gr)
			t_f = time.time()
			print("Time enumeration + : {}, obj enumeration: {}".format(str(t_f - t_d), res))
			obj_f.append(res)
			time_f.append(float(t_f - t_d))
			d = {"Algorithmes" : ["Enumeration", "Enumeration +"], "Temps" : time_f, "Objectif" : obj_f}
			i = i.split('.')[0][1:]
			df = pd.DataFrame(data=d)
			df.to_csv( i + "PourK=" + str(j) + ".csv", index=False)
			print(i + " create")

def testSimple(file, k):
	print("\n[[[[[[[[ K = " + str(k) + "]]]]]]]]")
	bridge = Event()
	tab_time = []
	obj = []
	gap_f = []
	obj_f = []
	time_f = []
	print("\n------------- " + file + "-------------\n")
	gr = Graph("sortedGraphes/" + file, k, bridge)
	s = gr.init_sol()
	#gr.bridge.set()
	t_d = time.time()
	#res = generateAndTest(gr)
	#res = gradient(gr, s, voisinnage)
	res = recuit(gr)
	t_f = time.time()
	print("Time : {}, obj : {}".format(str(t_f - t_d), res))
	t_d = time.time()
	#res = recuit(gr)
	res = tabou(gr)
	t_f = time.time()
	print("Time : {}, obj : {}".format(str(t_f - t_d), res))
	del bridge
	del gr


if __name__ == '__main__':
	path = "./sortedGraphes/"
	files = os.listdir(path)
	files.sort()
	if (files[0] == ".DS_Store"):
		del files[0]
	#print(files)
	#files = ["a4Sommets.txt","b5Sommets.txt", "c10Sommets.txt", "d15Sommets.txt", "e17Sommets.txt", "f20Sommets.txt"]
	#files = ["f20Sommets.txt", "g21Sommets.txt", "h22Sommets.txt", "i23Sommets.txt", "j24Sommets.txt", "k25Sommets.txt" ,"l30Sommets.txt"]
	#files = ["m50Sommets.txt","o500Sommets.txt","p1000Sommets.txt"]
	#files = ["q10000Sommets.txt"]

	#testEnum(files)
	#testSimple("q10000Sommets.txt", 2)
	testAll(files)
