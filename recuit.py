import random
from graph import *
from evaluator import *
from utils import *
from gradient import *


def init_temperature(graph, sol_init):
	"""
	Calcule de la temperature initiale

	:param temp: le graphe et une solution initiale
	:type temp: Graph, *int
	:return: la température calculee en fonction des valeurs des voisins
	:rtype: int
	"""
	delta = 0
	for v in voisinnageRestreint_random_swap(sol_init):
		delta += evaluateur(graph, v) - evaluateur(graph, sol_init)
	delta = delta / len(v)
	if(-delta / math.log(0.5) == 0):
		return 0.001
	return -delta / math.log(0.5)


def recuit(graph, s=None):
	"""
	Methode du recuit simule

	:param temp: le graphe et une solution initiale, par défaut la solution du gradient ameliore
	:type temp: Graph, *int
	:return: la valeur de la meilleure solution trouvee
	:rtype: int
	"""
	if (s == None):
		s = gradient(graph, graph.init_sol(), voisinnage)
	best = s
	fmin = evaluateur(graph, s)
	temperature = init_temperature(graph, s)
	t = 0
	i = 0
	while (i < 100):
		j = 0
		while (j < 100):
			s1 = pick_one_voisinnage(graph, s)
			fs1 = evaluateur(graph, s1)
			df = fs1 - evaluateur(graph, s)
			if (df < 0):
				s = s1
				if (fs1 < fmin):
					best = s1
					fmin = fs1
					j = 0
			else:
				p = random.randint(0,1)
				if (p < math.exp(-df/temperature)):
					s = s1
			if (graph.bridge and graph.bridge.is_set()):
				print("-- Recuit : Time out --")
				break
			j += 1
		temperature = graph.mu * temperature
		i += 1
	print("Recuit : sol : {}, eval : {}".format(best, fmin))
	return fmin
