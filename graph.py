from edge import *
import random
from utils import *
import math

class Graph():
	def load_by_file(self, filepath):
		"""
		Lecture d'une instance de graphe

		:param temp: la structure de graphe et le chemin du fichier
		:type temp: Graph, string
		"""
		with open(filepath,'r') as fd :
			i = 0
			lines = fd.readlines()
			while (lines[i][0] == '#'):
				i+=1
			lines[i] = lines[i].replace('\n', '')
			split = lines[i].split(' ')
			self.nbNodes = int(split[0])
			#self.degree = [None] * self.nbNodes
			self.nodes = [[] for i in range(self.nbNodes)]
			self.matrix = [[0 for i in range(self.nbNodes)] for i in range(self.nbNodes)]
			self.nbEdges = int(split[1])
			self.edges = []
			i+=1
			while (lines[i][0] == '#'):
				i+=1
			lines[i] = lines[i].replace('\n', '')
			split = lines[i].split(' ')
			#self.dmin = int(split[0])
			#self.dmax = int(split[1])
			i+=1
			while (lines[i][0] == '#'):
				i+=1
			while (lines[i][0] != '#'):
				line = lines[i].replace('\n', '')
				tmp = line.split(' ')
				begin = int(tmp[0]) - 1
				end = int(tmp[1]) - 1
				weight = int(tmp[2])
				self.edges.append(Edge(begin, end, weight))
				self.matrix[begin][end] = weight
				self.matrix[end][begin] = weight
				self.nodes[begin].append(end)
				self.nodes[end].append(begin)
				i+=1
			#while (lines[i][0] == '#'):
			#	i+=1
			#while (i < len(lines) and lines[i][0] != '#'):
			#	lines[i] = lines[i].replace('\n', '')
			#	split = lines[i].split(' ')
			#	self.degree[int(split[0]) - 1] = split[1]
			#	i+=1

	def __init__(self, filepath, k=None, bridge=None):
		self.load_by_file(filepath)
		if (k != None):
			self.k = k
		else:
			#print("sqrt(n) par défaut")
			self.k = math.ceil(math.sqrt(self.nbNodes))
		self.mu = float(random.randint(85, 95))/100.
		self.bridge = bridge

	def init_sol(self):
		"""
		Initialisation d'une solution valide
			Par exemple pour k=3 s=[0,1,2,0,1,2,0,...]

		:param temp: le graph
		:type temp: Graph
		:return: la solution generee
		:rtype: *int
		"""
		s = []
		i = 0
		while (i < self.nbNodes):
			s.append((i+self.k)%self.k)
			i+= 1
		return s

	def display(self):
		"""
		Affichage du contenu du graphe

		:param temp: le graph
		:type temp: Graph
		"""
		print("Sommets : {} | arretes : {}".format(self.nbNodes, self.nbEdges))
		#print("dmin : {} | dmax : {}".format(self.dmin, self.dmax))
		for i in self.edges:
			i.display()
		#for i in range(0, len(self.degree)):
		#	print("Sommet {} degree : {} ".format(i, self.degree[i]))
