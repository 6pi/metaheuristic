from evaluator import *

def enumeration(n, k):
	"""
	Enumeration de toutes les combinaisons possibles de [0]*n a [k]*n

	:param temp: taille des solutions et nombre de classe
	:type temp: int, int
	:return: la liste de toutes les solutions possibles
	:rtype: *int[n]
	"""
	tab = []
	current = [0]*n
	tab.append(current.copy())
	while current != [k-1]*n:
		i = 0
		while current[i] == k-1:
			current[i] = 0
			i += 1
		current[i] += 1
		tab.append(current.copy())
	return tab

def algoDeterministe(graph):
	"""
	Recherche de la meilleure solution réalisable
		parmi toutes les solutions possibles

	:param temp: structure de graphe et nombre de classe
	:type temp: Graph, int
	:return: valeur objective de la meilleure solution
	:rtype: int
	"""
	sol_best = graph.init_sol()
	obj_best = evaluateur(graph, sol_best)
	n = graph.nbNodes
	for s in enumeration(n, graph.k):
		obj_cur = evaluateur(graph, s)
		if test(graph, s) and obj_cur < obj_best:
			sol_best = s
			obj_best = obj_cur
		if graph.bridge and graph.bridge.is_set():
			print("--Deterministe : Time out--")
			print("Deterministe, sol : {}, eval : {}".format(sol_best, obj_best))
			return obj_best
	print("Deterministe, sol : {}, eval : {}".format(sol_best, obj_best))
	return obj_best

def generateAndTest(graph):
	"""
	Recherche de la meilleure solution réalisable
		parmi toutes les solutions valides

	:param temp: structure de graphe
	:type temp: Graph
	:return: valeur objective de la meilleure solution
	:rtype: int
	"""
	sol_best = graph.init_sol()
	obj_best = evaluateur(graph, sol_best)
	n = graph.nbNodes
	current = [0]*n
	while current[1:] != [graph.k-1]*(n-1):
		if test(graph, current):
			obj_cur = evaluateur(graph, current)
			if obj_cur < obj_best:
				sol_best = current.copy()
				obj_best = obj_cur
		i = 1
		while current[i] == graph.k-1:
			current[i] = 0
			i += 1
		current[i] += 1
	print("Deterministe, sol : {}, eval : {}".format(sol_best, obj_best))
	return obj_best
