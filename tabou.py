from graph import *
from utils import *
from enumeration import *
from evaluator import *
from gradient import *
import math


def voisinnage_tabou(graph, x, tab, len_max, o):
	"""
	Trouve la meilleure solution parmi une partie des voisins
		si le mouvement pour y arriver n'est pas tabou

	:param temp: un graphe, une solution, une liste de solution tabou, la taille max de ce tableau et la valeur de x
	:type temp: Graph, *int, **int, int, int
	:return: une solution, sa valeur et le mouvement pour passer de x à cette solution
	:rtype: *int, int, (int, int)
	"""
	v_list = voisinnageRestreint_random_swap(x)
	sol_best = v_list[0][0]
	mvt_best = v_list[0][1]
	obj_best = evaluateur_improve(graph, x, sol_best, o, mvt_best)
	for c in v_list:
		v = c[0]
		mvt = c[1]
		obj_cur = evaluateur_improve(graph, x, v, o, mvt)
		if obj_cur < obj_best:
			if not (mvt in tab):
				mvt_best = mvt
				sol_best = v
				obj_best = obj_cur
	return sol_best, obj_best, mvt_best


def tabou(graph, len_max=7, x_best=None):
	"""
	Methode tabou

	:param temp: un graphe, une taille maximale défini par défaut à 7 et une solution initiale défini par défaut comme la solution du gradient amélioré
	:type temp: Graph, (int), (*int)
	:return: la valeur de la solution trouvée
	:rtype: int
	"""
	if (x_best == None):
		x_best = gradient(graph, graph.init_sol(), voisinnage)
		x_best = x_best[0]
	f_min = evaluateur(graph, x_best)
	f_y = f_min
	y = x_best
	tabou = []
	iter = 0
	j = 0
	while (j < max(graph.nbNodes, 100)):
		y, f_y, mvt = voisinnage_tabou(graph, y, tabou, len_max, f_y)
		if (f_y < f_min):
			x_best = y
			f_min = f_y
			iter = 0
		tabou.append(mvt)
		if (len(tabou) >= len_max):
			tabou = tabou[1:len_max]
		if (graph.bridge and graph.bridge.is_set()):
			print("-- Tabou : Time out --")
			break
		j += 1
		iter += 1
	print("Tabou : sol : {}, eval : {}".format(x_best, f_min))
	return (f_min)
