# MetaHeuristic
Cyprien BOYER
Chloe GUIMBERTEAU

## Compilation
Version minimum requise Python 3.7.3
'''
$python3 main.py
'''

## Sujet
Projet de partitionnement de graphe en Python

Entree : graphe G=(U,E,w) contenu dans un fichier .txt
          k un entier dans {2,3,sqrt(n)}
Sortie : tableau de taille card(U) contenant le numero de la classe de chaque sommet

Objectif : Partitionner U en k classe a peu pres equitables de telle sorte que la somme des poids des aretes inter-classes soit minimale

Notion d'a peu pres equitable =
 cardinal de chaque classe a peu pres egale, U/k +- arrondi_sup(5% de U)

* Implementer un algo deterministe, pour les instances de moins de 30 sommets
* Methode du gradient standard
* 2 meta-heuristiques : Recuit simule et Tabou

## Ressources

Page de cours : https://sites.google.com/site/marcmichelcorsini/MIMSE

## État du projet
-> Structure de graphe et lecture de données
-> Énumération et Gradient
-> Recuit simulé
-> Tabou
-> Tests des algos en temps
--> Comparaison des algos avec paramètres différents
--> Analyse des résultats
---> Rédaction du rapport

## Rendu pour le 18/04/2022

## Lien du rapport
Lecture : <https://www.overleaf.com/read/ytbzhhzwqndp>
Edition : <https://www.overleaf.com/7234378868kpcrjvxyvsyr>
