import math
from utils import *

def test(graph, s, verbose=False):
	"""
	Teste si la solution s est valide,
	c'est-a-dire si les k classes de la solution sont a peu pres equitables
	 Card(classe) = U/k +- arrondi_sup(0.05*U)

	:param temp: le graphe, la solution et un booleen pour l'affichage défini par défaut à faux
	:type temp: Graph, *int, bool
	:return: si la solution est valide
	:rtype: bool
	"""
	if (len(s) == 2):
		s = s[0]
	sup = graph.nbNodes/graph.k + math.ceil(graph.nbNodes * 0.05)
	inf = graph.nbNodes/graph.k - math.ceil(graph.nbNodes * 0.05)
	tab = [0]*graph.k
	for i in s:
		tab[i] += 1
	for i in tab:
		if i == 0 or i > sup or i < inf:
			return False
	if verbose:
		print(tab)
	return True

def evaluateur(graph, s):
	"""
	Fonction d'évaluation qui calcule la somme des poids des aretes inter-classes

	:param temp: le graph et la solution
	:type temp: Graph, *int
	:return: la valeur de la solution
	:rtype: int
	"""
	sol = 0
	if (len(s) == 2):
		s = s[0]
	for e in graph.edges:
		if (s[e.begin] != s[e.end]):
			sol += e.weight
	return sol

def evaluateur_improve(graph, s, v, sol, mvt):
	"""
	Fonction d'évaluation qui calcule la somme des poids des aretes inter-classes
		d'une nouvelle solution depuis une solution précédente

	:param temp: le graph, la solution précédente, la solution actuelle, la valeur de la solution précédente et le mouvement
	:type temp: Graph, *int, *int, int, (int,int)
	:return: la valeur de la solution actuelle v
	:rtype: int
	"""
	if (len(s) == 2):
		s = s[0]
	for e in mvt:
		for i in graph.nodes[e]:
			if (s[i] != s[e]):
				sol -= graph.matrix[e][i]
			if (v[i] != v[e]):
				sol += graph.matrix[e][i]
	return sol
