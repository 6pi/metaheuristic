from graph import *
from evaluator import *

def random_pick_two_different(s):
	"""
	Renvoit deux indices de s aléatoire et différent

	:param temp: une solution
	:type temp: *int
	:return: les deux entiers
	:rtype: int, int
	"""
	i = random.randint(0, len(s) - 1)
	j = random.randint(0, len(s) - 1)
	while (s[i] == s[j]):
		j = random.randint(0, len(s) - 1)
	return (i, j)

def swap(s, i, j):
	"""
	Échange les valeurs de s en position i et j

	:param temp: une solution, deux indices
	:type temp: *int, int, int
	:return: la solution modifiée et le mouvement effectué
	:rtype: *int, (int, int)
	"""
	s2 = s.copy()
	tmp = s2[i]
	s2[i] = s2[j]
	s2[j] = tmp
	return (s2,(i,j))

def random_swap(s):
	"""
	Échange les valeurs de s en position i et j tirés aléatoirement

	:param temp: une solution
	:type temp: *int
	:return: la solution modifiée et le mouvement effectué
	:rtype: *int, (int, int)
	"""
	if (len(s) == 2):
		s = s[0]
	(i, j) = random_pick_two_different(s)
	v = s.copy()
	v[i] = s[j]
	v[j] = s[i]
	return (v,(i,j))

def random_sweep(s):
	"""
	Décale les valeurs de s qui se trouvent à une position entre i et j tirés aléatoirement

	:param temp: une solution
	:type temp: *int
	:return: la solution modifiée
	:rtype: *int
	"""
	(i, j) = random_pick_two_different(s)
	if (len(s) == 2):
		s = s[0]
	v = s.copy()
	cur = 0
	while (cur < len (s)):
		v[cur] = s[(i+cur)%(len(s))]
		cur += 1
	return v

def random_pick_n_drop(graph, s):
	"""
	Modifie la valeur de s en position i tiré aléatoirement
		pour une autre valeur disponible tirée aléatoirement

	:param temp: un graphe et une solution
	:type temp: Graph, *int
	:return: la solution modifiée
	:rtype: *int
	"""
	if (len(s) == 2):
		s = s[0]
	v = s.copy()
	z = True
	k = graph.k
	while (z or test(graph, v) == False):
		i = random.randint(0, len(s) - 1)
		c = random.randint(0, k - 1)
		while (v[i] == c):
			c = random.randint(0, k - 1)
		v[i] = c
		z = False
	return v

def pick_one_voisinnage(graph, s):
	"""
	Renvoit une solution voisine tirée aléatoire parmi les trois fonctions

	:param temp: un graphe et une solution
	:type temp: Graph, *int
	:return: la solution modifiée
	:rtype: *int
	"""
	p = random.randint(0, 2)
	if (p == 0):
		v = random_swap(s)
	if (p == 1):
		v = random_sweep(s)
	if (p == 2):
		v = random_pick_n_drop(graph, s)
	return v

def voisinnage(s):
	"""
	Énumère l'ensemble des solutions voisines à s par la fonction swap

	:param temp: une solution
	:type temp: *int
	:return: la liste de tous les voisins
	:rtype: **int
	"""
	v = []
	i = 0
	while (i < len(s)):
		if (s[i] == 1):
			j = 0
			while (j < len(s)):
				if (s[j] != s[i]):
					v.append(swap(s,i,j))
				j+=1
		i+=1
	return v

'''
def voisinnageRestreint(s):
	v = []
	i = 0
	while (i < len(s) and len(v) < 100):
		if (s[i] == 1):
			j = 0
			while (j < len(s) and len(v) < 100):
				if (s[j] != s[i]):
					v.append(swap(s,i,j))
				j+=1
		i+=1
	return v
'''
def voisinnageRestreint_random_swap(s):
	"""
	Énumère un ensemble restreint des solutions voisines à s par la fonction random_swap,
		restreint par une taille de minimum 100 ou le nombre de sommets dans le graph

	:param temp: une solution
	:type temp: *int
	:return: la liste des premiers voisins trouvés
	:rtype: **int
	"""
	v = []
	if (len(s) == 2):
		s = s[0]
	len_s = len(s)
	n = len_s if (len_s > 100) else 100
	i = 0
	while (len(v) != n and i < len_s * len_s):
		tmp = random_swap(s)
		i+=1
		v.append(tmp)
	return v
