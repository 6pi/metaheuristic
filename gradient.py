from evaluator import *
from utils import *
import random

def gradient_standard(graph, s, fct_vois=voisinnage):
	"""
	Methode du gradient standard
		qui prend la meilleure solution parmis tous ses voisins à chaque itération

	:param temp: le graphe, la solution et une fonction de voisinnage
	:type temp: Graph, *int, *int[*int](*int)
	:return: la solution et sa valeur
	:rtype: *int, int
	"""
	obj_best = evaluateur(graph, s)
	sol_best = s
	v_list = fct_vois(s)
	for v in v_list:
		obj_cur = evaluateur(graph, v)
		if obj_cur < obj_best:
			obj_best = obj_cur
			sol_best = v
		if graph.bridge and graph.bridge.is_set():
			print("--Gradient : Time out--")
			print("Gradient standard, sol : {}, eval : {}".format(sol_best, obj_best))
			return sol_best
	if obj_best != evaluateur(graph, s):
		return gradient_standard(graph, sol_best, fct_vois)
	else :
		print("Gradient standard, sol : {}, eval : {}".format(sol_best, obj_best))
		return sol_best, obj_best


def gradient(graph, s, fct_vois):
	"""
	Methode du gradient ameliore
		qui prend la première solution ameliorante parmis ses voisins

	:param temp: le graphe, la solution et une fonction de voisinnage
	:type temp: Graph, *int, *int[*int](*int)
	:return: la solution et sa valeur
	:rtype: *int, int
	"""
	obj_best = evaluateur(graph, s)
	sol_best = s
	o = obj_best
	v_list = fct_vois(s)
	random.shuffle(v_list)
	for v in v_list:
		obj_cur = evaluateur_improve(graph, s, v[0], o, v[1])
		if obj_cur < obj_best:
			obj_best = obj_cur
			sol_best = v[0]
			return gradient(graph, sol_best, fct_vois)
		if graph.bridge and graph.bridge.is_set():
			print("--Gradient : Time out--")
			print("Gradient, sol : {}, eval : {}".format(sol_best, obj_best))
			return sol_best
	if obj_best != o:
		return gradient(graph, sol_best, fct_vois)
	else :
		print("Gradient, sol : {}, eval : {}".format(sol_best, obj_best))
		return sol_best, obj_best
